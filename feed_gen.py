#!/usr/bin/env python3
#
# MP3 Nocni dharma downloader
# ===========================
# - download all nocni dharma talks from radio1 archiv to current directory
# - require python3
# - tested only on Linux OS
#
# Marek Sirovy (msirovy _at_ gmail.com)


from urllib.request import urlopen, Request
import re, jinja2
from os import path, curdir, sys
from datetime import datetime
from pprint import pprint

URL="http://www.radio1.cz/archiv-poradu/download-list/35-pulnocni-dharma?perpage=500"
TEMPLATE="./rss_feed.j2"
FEED_PATH="./pulnocni_dharma.rss"

def save_mp3(url, file_name):
    """ Get url and save it to file as binary stream
    """
    with open(file_name, 'wb') as F:
        req = Request(url)
        data = urlopen(req).read()
        F.write(data)
    return True


def full_date_of_date(date_string, date_format="%Y%m%d"):
    #print("  >>>> ", date_string)
    time_stamp = datetime.strptime(date_string, date_format).strftime('%a, %d %b %Y %T +0200')
    return str(time_stamp)


def gen_feed_item(url):
    """ Generate rss feed item for given url
    """
    try:
        """ If url exists give size of file
        """
        link = urlopen(url).info()
        _ret = dict()
        _ret['length'] = link.get("Content-Length")
        _ret['name'] = link.get_filename().replace('-',' ')[:-4].capitalize()
        _ret['link'] = url
        _ret['type'] = "audio/mpeg"
        _ret['description'] = _ret['name']
        _ret['pub_date'] = str(full_date_of_date("".join(_ret['name'].split()[-3:])))
        print(" OK: ", url)
        return _ret

    except ValueError as Err:
        print(" FAIL: ", url)
        print(Err)
        pprint(_ret)
        return False


def render_template(tpl_path, data):
    _path, _filename = path.split(tpl_path)
    return jinja2.Environment(
                    loader=jinja2.FileSystemLoader(_path or './')
                    ).get_template(_filename).render(data)


if __name__ == '__main__':
    
    _req = Request(URL)
    _WORK_DIR = path.abspath(curdir)
    
    try:
        FEED_PATH = sys.argv[1]
    except IndexError:
        print("Save feed to default file %s" % FEED_PATH)
         
    episodes = list()
    for link in re.findall( r'<a href="/archiv-poradu/(.*?)mp3">', str(urlopen(_req).read()) )[::-1]:
        """ Iterate over all parsed mp3 links from given url
            and store them as dharma-ROK-MESIC_DEN.mp3 files
        """
        episodes.append(gen_feed_item("http://www.radio1.cz/archiv-poradu/%smp3.mp3" % link))

    now = datetime.now().strftime('%a, %d %b %Y %T +0200')
    print(now)
    with open(FEED_PATH, "w") as F:
        F.write((render_template(TEMPLATE, dict(data = episodes, build_date = now))))

    print("Generation finished...\n See result in %s" % FEED_PATH)

